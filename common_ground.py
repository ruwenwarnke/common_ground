
# please change spotify client id and secret as well as spotify user name and token...


#TODO
# generate new token automatically from refresh token
# expand similarities based on genre composition
# optimize SQL
# add artist input
# add track input
# add authorization input
# add other STreaming services
# build html frontend
# add gifs where computing
# test different genres
# utilize Paul Lamere API for shortest path (http://static.echonest.com/BoilTheFrog/)



import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import pandas as pd


SPOTIPY_CLIENT_ID='XXXXXXXXXXXXXXX'
SPOTIPY_CLIENT_SECRET='XXXXXXXXXXXXXXXXXXXXXX'
SPOTIPY_REDIRECT_URI='XXXXXXXXXXXXXXXX'

client_credentials_manager = SpotifyClientCredentials(SPOTIPY_CLIENT_ID, SPOTIPY_CLIENT_SECRET)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)


p1 = input('paste a playlist URI for your music taste: ')[34:]
p2 = input('paste a playlist URI for another persons music taste: ')[34:]


tracks_1= sp.user_playlist('11177351122', playlist_id=p1, fields=None)['tracks']['items']
tracks_2= sp.user_playlist('11177351122', playlist_id=p2, fields=None)['tracks']['items']

# get artists from playlist
artists_1 = []
artists_2 = []
for track in tracks_1:
    for artist in track['track']['artists']:
        artists_1.append(artist['id'])

for track in tracks_2:
    for artist in track['track']['artists']:
        artists_2.append(artist['id'])

#communicate
common_artists = list(set(artists_1) & set(artists_2))
print('')
if len(common_artists) < 5:
    print('You two have '+str(len(common_artists))+' artists in common!. You guys really have no common ground!')
    print('looks like I have to dig a little deeper!...')
else:
    print('You two have '+str(len(common_artists)) + ' artists in common!. That should be just fine!')
    print('Let me mix you giys a nice playlist!')


def get_similar_artists(artist_list):
    query = """
    SELECT      target as ac_id, count(*) as cnt
    FROM        [umg-de:artistcluster.ac_edges] 
    WHERE       source in
        (SELECT      id
        FROM        [umg-de:artistcluster.ac_artists] 
        WHERE       spotify_id in ("""+str(artist_list)[1:-1]+"""))
    GROUP BY ac_id
    """
    return pd.read_gbq(query, project_id='umg-de', dialect='legacy')

def get_similar_artists_ac(artist_list):
    query = """
    SELECT      target as ac_id, count(*) as cnt
    FROM        [umg-de:artistcluster.ac_edges] 
    WHERE       source in  ("""+str(artist_list)[1:-1]+""")
    GROUP BY ac_id
    """
    return pd.read_gbq(query, project_id='umg-de', dialect='legacy')

def get_spotify_ids(ac_ids):
    query = """
    SELECT      spotify_id
    FROM        [umg-de:artistcluster.ac_artists] 
    WHERE       id in  ("""+str(ac_ids)[1:-1]+""")
    """
    return pd.read_gbq(query, project_id='umg-de', dialect='legacy')

#find diminant cluster
def find_diminant_cluster(common_artists):
    query = """
    SELECT id
    FROM [umg-de:artistcluster.ac_nodes]
    WHERE modularity in 
        (SELECT modularity
        FROM 
            (SELECT      modularity, count(*) as cnt
            FROM        [umg-de:artistcluster.ac_nodes] 
            WHERE       id in  ("""+str(common_artists)[1:-1]+""")
            GROUP BY    modularity
            ORDER BY    cnt DESC)
        LIMIT 1)
    LIMIT 5
    """
    return pd.read_gbq(query, project_id='umg-de', dialect='legacy')

if len(common_artists)<5:
    print('...')
    print('still no matches, tough one!')
    artists_1=get_similar_artists(artists_1)['ac_id'].values.tolist()
    artists_2 = get_similar_artists(artists_2)['ac_id'].values.tolist()
    common_artists = list(set(artists_1) & set(artists_2))
    while len(common_artists) <5:
        print('...')
        print('Damn! Your taste in music coud not be more different!...')
        print('No worries, I will find something...')
        artists_1 = get_similar_artists_ac(artists_1)['ac_id'].values.tolist()
        artists_2 = get_similar_artists_ac(artists_2)['ac_id'].values.tolist()
        common_artists = list(set(artists_1) & set(artists_2))
        print(len(common_artists))
    common_artists=(find_diminant_cluster(common_artists)['id'].values.tolist())
    common_artists=get_spotify_ids(common_artists)['spotify_id'].values.tolist()


#scope = '...'
#token = util.prompt_for_user_token('11177351122', scope, SPOTIPY_CLIENT_ID, SPOTIPY_CLIENT_SECRET,SPOTIPY_REDIRECT_URI)
# enter your token here
token='XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
sp = spotipy.Spotify(auth=token)

#get recommendations for artist
track_list = []
for track in sp.recommendations(seed_artists=common_artists[0:5], target_danceability=1, target_valence=1)['tracks']:
    track_list.append(track['id'])

#create playlist
playlist_id=(sp.user_playlist_create('11177351122','common ground'))['id']

#add tracks
sp.user_playlist_add_tracks('11177351122', playlist_id, track_list, position=None)

print('')
print('Got it! Check it out on your Spotify app!')